//header file containing stack method definitions
#ifndef CSTACK_H_
#define CSTACK_H_

void init();
bool isEmpty();
void push(int inp);
int pop();
int size();

#endif //cstack.h
