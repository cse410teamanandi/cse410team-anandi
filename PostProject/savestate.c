#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <stdbool.h>
#include "include/structures.h"

int save_called = 0;

bool chk_file(const char *filname){
    FILE *infile;
    if ((infile = fopen(filname, "r"))){
        fclose(infile);
        return true;
    }
    return false;
}

void save(char *origin, char *filename, struct Product * currentProduct, struct Filter * currentFilter) {
  if (chk_file(filename) == true) {
    fprintf(stderr, "[ERROR] Invalid Command : File Already Exists (Try using overwrite?)\n");
  }
  else {
    FILE *fp;
    fp = fopen(filename, "w");
    if(currentFilter!=NULL) {
      fputs("<SEARCH>\n",fp);
      fprintf(fp, "%s%s%s\n", "<BASE>", origin, "</BASE>");
      currentFilter = currentFilter->head;
      while(currentFilter!=NULL) {
        fprintf(fp, "%s%s%s%s%s%s%s\n", "<FILTER><", currentFilter->field, ">", currentFilter->keyword, "</", currentFilter->field, "></FILTER>");
        currentFilter = currentFilter->next;
      }
      fputs("</SEARCH>\n",fp);
      while(currentProduct!=NULL) {
        fputs("<PRODUCT>\n",fp);
        fprintf(fp, "%s%s%s\n", "<DATE>", currentProduct->date, "</DATE>");
        fprintf(fp, "%s%s%s%s%s\n", "<BRAND_NAME>", "<![CDATA[", currentProduct->name, "]]>", "</BRAND_NAME>");
        fprintf(fp, "%s%s%s%s%s\n", "<PRODUCT_DESCRIPTION>", "<![CDATA[", currentProduct->description, "]]>", "</PRODUCT_DESCRIPTION>");
        fprintf(fp, "%s%s%s%s%s\n", "<REASON>", "<![CDATA[", currentProduct->reason, "]]>", "</REASON>");
        fprintf(fp, "%s%s%s%s%s\n", "<COMPANY>", "<![CDATA[", currentProduct->company, "]]>", "</COMPANY>");
        fprintf(fp, "%s%s%s\n", "<COMPANY_RELEASE_LINK>", currentProduct->release, "</COMPANY_RELEASE_LINK>");
        fprintf(fp, "%s%s%s\n", "<PHOTOS_LINK>", currentProduct->photos, "</PHOTOS_LINK>");
        fputs("</PRODUCT>\n",fp);
        currentProduct = currentProduct->next;
      }
      save_called += 1;
    }
    else {
      while(currentProduct!=NULL) {
        fputs("<PRODUCT>\n",fp);
        fprintf(fp, "%s%s%s\n", "<DATE>", currentProduct->date, "</DATE>");
        fprintf(fp, "%s%s%s%s%s\n", "<BRAND_NAME>", "<![CDATA[", currentProduct->name, "]]>", "</BRAND_NAME>");
        fprintf(fp, "%s%s%s%s%s\n", "<PRODUCT_DESCRIPTION>", "<![CDATA[", currentProduct->description, "]]>", "</PRODUCT_DESCRIPTION>");
        fprintf(fp, "%s%s%s%s%s\n", "<REASON>", "<![CDATA[", currentProduct->reason, "]]>", "</REASON>");
        fprintf(fp, "%s%s%s%s%s\n", "<COMPANY>", "<![CDATA[", currentProduct->company, "]]>", "</COMPANY>");
        fprintf(fp, "%s%s%s\n", "<COMPANY_RELEASE_LINK>", currentProduct->release, "</COMPANY_RELEASE_LINK>");
        fprintf(fp, "%s%s%s\n", "<PHOTOS_LINK>", currentProduct->photos, "</PHOTOS_LINK>");
        fputs("</PRODUCT>\n",fp);
        currentProduct = currentProduct->next;
      }
      save_called +=1;
    }
  }
}

void overwrite(char *origin, char *filename, struct Product * currentProduct, struct Filter * currentFilter) {
  if (chk_file(filename) == false) {
    fprintf(stderr, "[ERROR] Invalid Command : File DNE (Try using save?)\n");
  }
  else {
    FILE *fp;
    fp = fopen(filename, "w");
    if(currentFilter!=NULL) {
      fputs("<SEARCH>\n",fp);
      fprintf(fp, "%s%s%s\n", "<BASE>", origin, "</BASE>");
      currentFilter = currentFilter->head;
      while(currentFilter!=NULL) {
        fprintf(fp, "%s%s%s%s%s%s%s\n", "<FILTER><", currentFilter->field, ">", currentFilter->keyword, "</", currentFilter->field, "></FILTER>");
        currentFilter = currentFilter->next;
      }
      fputs("</SEARCH>\n",fp);
      while(currentProduct!=NULL) {
        fputs("<PRODUCT>\n",fp);
        fprintf(fp, "%s%s%s\n", "<DATE>", currentProduct->date, "</DATE>");
        fprintf(fp, "%s%s%s%s%s\n", "<BRAND_NAME>", "<![CDATA[", currentProduct->name, "]]>", "</BRAND_NAME>");
        fprintf(fp, "%s%s%s%s%s\n", "<PRODUCT_DESCRIPTION>", "<![CDATA[", currentProduct->description, "]]>", "</PRODUCT_DESCRIPTION>");
        fprintf(fp, "%s%s%s%s%s\n", "<REASON>", "<![CDATA[", currentProduct->reason, "]]>", "</REASON>");
        fprintf(fp, "%s%s%s%s%s\n", "<COMPANY>", "<![CDATA[", currentProduct->company, "]]>", "</COMPANY>");
        fprintf(fp, "%s%s%s\n", "<COMPANY_RELEASE_LINK>", currentProduct->release, "</COMPANY_RELEASE_LINK>");
        fprintf(fp, "%s%s%s\n", "<PHOTOS_LINK>", currentProduct->photos, "</PHOTOS_LINK>");
        fputs("</PRODUCT>\n",fp);
        currentProduct = currentProduct->next;
      }
      save_called += 1;
    }
    else {
      while(currentProduct!=NULL) {
        fputs("<PRODUCT>\n",fp);
        fprintf(fp, "%s%s%s\n", "<DATE>", currentProduct->date, "</DATE>");
        fprintf(fp, "%s%s%s%s%s\n", "<BRAND_NAME>", "<![CDATA[", currentProduct->name, "]]>", "</BRAND_NAME>");
        fprintf(fp, "%s%s%s%s%s\n", "<PRODUCT_DESCRIPTION>", "<![CDATA[", currentProduct->description, "]]>", "</PRODUCT_DESCRIPTION>");
        fprintf(fp, "%s%s%s%s%s\n", "<REASON>", "<![CDATA[", currentProduct->reason, "]]>", "</REASON>");
        fprintf(fp, "%s%s%s%s%s\n", "<COMPANY>", "<![CDATA[", currentProduct->company, "]]>", "</COMPANY>");
        fprintf(fp, "%s%s%s\n", "<COMPANY_RELEASE_LINK>", currentProduct->release, "</COMPANY_RELEASE_LINK>");
        fprintf(fp, "%s%s%s\n", "<PHOTOS_LINK>", currentProduct->photos, "</PHOTOS_LINK>");
        fputs("</PRODUCT>\n",fp);
        currentProduct = currentProduct->next;
      }
      save_called += 1;
    }
  }
}
