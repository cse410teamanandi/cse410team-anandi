#include "include/structures.h"
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>


int prodCount = 0;
int filtCount = 0;

struct Product *
create_product(char* date, char* name, char* description, char* reason,
  char* company, char* release, char* photos) {

    struct Product *p;
    p = malloc(sizeof(*p));
    p->date = date;
    p->name = name;
    p->description = description;
    p->reason = reason;
    p->company = company;
    p->release = release;
    p->photos = photos;
    p->next = NULL;
    p->head = p;
    prodCount++;
    return p;
  }

  void addProductToList(struct Product *initialProduct, struct Product *newProduct){
    while (initialProduct->next != NULL){
      initialProduct = initialProduct->next;
    }
    initialProduct->next = newProduct;
    newProduct->head = initialProduct->head;
    initialProduct = initialProduct->head;
  }

  void printProduct(struct Product *inProduct) {
    inProduct = inProduct->head;
    printf("STARTING print!!\n");
    while(inProduct) {
      printf("START_OF_PRODUCT\n");
      printf("%s\n", inProduct->date);
      printf("%s\n", inProduct->name);
      printf("%s\n", inProduct->description);
      printf("%s\n", inProduct->reason);
      printf("%s\n", inProduct->company);
      printf("%s\n", inProduct->release);
      printf("%s\n", inProduct->photos);
      printf("END_OF_PRODUCT\n");
      // struct Product *nextProduct = inProduct->next;
      inProduct = inProduct->next;
    }
  }

  void addFilterToList(struct Filter *initialFilter, struct Filter *newFilter){
    initialFilter = initialFilter->head;
    if (initialFilter->next == NULL) {
      initialFilter->next = newFilter;
      newFilter->head = initialFilter->head;
    }
    else {
      struct Filter* currentFilter = initialFilter->head;

      while (true){
        if (currentFilter->next == NULL){
          currentFilter->next = newFilter;
          currentFilter->next->head = currentFilter->head;
          break;
        }
        currentFilter = currentFilter->next;
      }
    }
  }

  struct Filter *create_filter(char* keyword, char* field){
    struct Filter *f;
    f = malloc(sizeof(*f));
    f->keyword = keyword;
    f->field = field;
    f->head = f;
    f->next = NULL;
    filtCount++;

    return f;
  }

  struct Product *add_filter(struct Product *initialProducts,
    struct Filter *filterList, char *keyword, char *field, bool in){

      struct Filter *newFilter = create_filter(keyword,field);
      struct Product *filteredProducts;
      filteredProducts = NULL;
      int newSize = 0;

      // check all keywords for a single match
      if (strstr(field,"ALL") != NULL){
        while (initialProducts != NULL){
          if (strstr(initialProducts->date,keyword)     == NULL ||
          strstr(initialProducts->name,keyword)         == NULL ||
          strstr(initialProducts->description,keyword)  == NULL ||
          strstr(initialProducts->reason,keyword)       == NULL ||
          strstr(initialProducts->company,keyword)      == NULL ||
          strstr(initialProducts->release,keyword)      == NULL ||
          strstr(initialProducts->photos,keyword)       == NULL)
          {
            // does not contain, skip entire product!

            // ALL, in - Skip current Product since the keyword not in all fields
            if (in){
              initialProducts = initialProducts->next;
            }
            // ALL, notin - add current Product since the keyword not in all fields
            else {
              struct Product *newProduct =
              create_product(initialProducts->date,initialProducts->name,
                initialProducts->description, initialProducts->reason,
                initialProducts->company,initialProducts->release,
                initialProducts->photos);

              if (filteredProducts == NULL){
                filteredProducts = newProduct;
              }
              else {
                addProductToList(filteredProducts,newProduct);
                initialProducts = initialProducts->next;
                if (newSize == 0){
                  filteredProducts->head = newProduct;
                  newProduct->head = newProduct;
                }
              }
              newSize++;
            }
          }

          else {
            // ALL, in - Add current Product since keyword is in all fields.
            if (in){
              struct Product *newProduct =
              create_product(initialProducts->date,initialProducts->name,
                initialProducts->description, initialProducts->reason,
                initialProducts->company,initialProducts->release,
                initialProducts->photos);

                if (filteredProducts == NULL){
                  filteredProducts = newProduct;
                }
                else {
                  addProductToList(filteredProducts,newProduct);
                  initialProducts = initialProducts->next;
                  if (newSize == 0){
                    filteredProducts->head = newProduct;
                    newProduct->head = newProduct;
                  }
                }
                newSize++;
              }
              // ALL, notin - Skip current Product since keyword in all fields.
              else {
                initialProducts = initialProducts->next;
              }
            }
          }
        }

          //check all fields for all matches
        else if (strstr(field,"ANY") != NULL){

          while (initialProducts != NULL){

            if (strstr(initialProducts->date,keyword)     != NULL ||
            strstr(initialProducts->name,keyword)         != NULL ||
            strstr(initialProducts->description,keyword)  != NULL ||
            strstr(initialProducts->reason,keyword)       != NULL ||
            strstr(initialProducts->company,keyword)      != NULL ||
            strstr(initialProducts->release,keyword)      != NULL ||
            strstr(initialProducts->photos,keyword)       != NULL)
            {

              // ANY, in - keyword appeared in one of the fields
              if (in){
              // at least 1 contains the keyword, add product!
                struct Product *newProduct =
                create_product(initialProducts->date,initialProducts->name,
                  initialProducts->description, initialProducts->reason,
                  initialProducts->company,initialProducts->release,
                  initialProducts->photos);

                if (filteredProducts == NULL){
                  filteredProducts = newProduct;

                }
                else {
                  addProductToList(filteredProducts,newProduct);
                }
                initialProducts = initialProducts->next;
                newSize++;
              }

              // ANY, notin - keyword appeared, so don't add product
              else {
                initialProducts = initialProducts->next;
              }
            }
            else {

              // ANY, in - keyword did not appear in
              if (in){
                initialProducts = initialProducts->next;
              }

              // ANY, notin - kewyord did not appear in, so add Product to list.
              else {
                struct Product *newProduct =
                create_product(initialProducts->date,initialProducts->name,
                  initialProducts->description, initialProducts->reason,
                  initialProducts->company,initialProducts->release,
                  initialProducts->photos);

                  if (filteredProducts == NULL){
                    filteredProducts = newProduct;

                  }
                  else {
                    addProductToList(filteredProducts,newProduct);
                  }
                  initialProducts = initialProducts->next;
                  newSize++;
                }
              }
            }
            prodCount = newSize;
            filtCount++;

          }

          // field is specific
          else {
            char* curField;
            while (initialProducts != NULL){
              if (strcmp(field, "DATE") == 0){
                curField = initialProducts->date;
              }
              else if (strcmp(field, "BRAND_NAME") == 0){
                curField = initialProducts->name;
              }
              else if (strcmp(field, "DESCRIPTION") == 0){
                curField = initialProducts->description;
              }
              else if (strcmp(field, "REASON") == 0){
                curField = initialProducts->reason;
              }
              else if (strcmp(field, "COMPANY") == 0){
                curField = initialProducts->company;
              }
              else if (strcmp(field, "RELEASE") == 0){
                curField = initialProducts->release;
              }
              else if (strcmp(field, "PHOTOS") == 0){
                curField = initialProducts->photos;
              }
              else {
                // SHOULD NEVER BE REACHED
              }
              if (strstr(curField,keyword) != NULL){
                if (strstr(curField,"badcall") != NULL){
                  printf("%s\n", "Invalid field name. Type help for options");
                }
                else if (in){
                  struct Product *newProduct =
                  create_product(initialProducts->date,initialProducts->name,
                    initialProducts->description, initialProducts->reason,
                    initialProducts->company,initialProducts->release,
                    initialProducts->photos);

                    if (filteredProducts == NULL){
                      filteredProducts = newProduct;
                    }
                    else {
                      addProductToList(filteredProducts,newProduct);
                    }
                    initialProducts = initialProducts->next;
                    newSize++;
                }
                else {
                  initialProducts = initialProducts->next;
                }
              }

              else {
                if (in){
                  initialProducts = initialProducts->next;
                }
                else{
                  struct Product *newProduct =
                  create_product(initialProducts->date,initialProducts->name,
                    initialProducts->description, initialProducts->reason,
                    initialProducts->company,initialProducts->release,
                    initialProducts->photos);

                    if (filteredProducts == NULL){
                      filteredProducts = newProduct;

                    }
                    else {
                      addProductToList(filteredProducts,newProduct);
                    }
                    initialProducts = initialProducts->next;
                    newSize++;
                }
              }
            }
          }


          if (filterList != NULL) {
            addFilterToList(filterList,newFilter);
          }
          else {
            filterList = newFilter;
          }

          if (filteredProducts == NULL){
            return NULL;
          }
          return filteredProducts->head;
        }
