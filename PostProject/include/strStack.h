#ifndef STRSTACK_H
#define STRSTACK_H

typedef struct snode{
  char *str;
  struct snode *next;
}node;

typedef struct {
  node *head;
  int stackSize;
}stack;

stack *init();
char* copy(char *inp);
void push(stack *theStack, char *value);
char* top(stack *theStack);
char* pop(stack *theStack);
void clear(stack *theStack);
void destStack(stack **theStack);

#endif
