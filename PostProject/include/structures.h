#ifndef STRUCTURES_H_
#define STRUCTURES_H_

#include <stdbool.h>

struct Product {
  char *date;
  char *name;
  char *description;
  char *reason;
  char *company;
  char *release;
  char *photos;
  struct Product *head;
  struct Product *next;
};

struct Product *create_product(char* date, char* name, char* description,
                               char* reason, char* company, char* release,
                               char* photos);

void addProductToList(struct Product *initialProduct, struct Product *newProduct);

void printProduct(struct Product *inProduct);

struct Filter {
  char *keyword;
  char *field;
  struct Filter *head;
  struct Filter *next;
};

void addFilterToList(struct Filter *initialFilter, struct Filter *newFilter);

struct Filter *create_filter();

struct Product *add_filter(struct Product *initialProducts, struct Filter *filterList, char *keyword, char *field, bool in);

#endif
