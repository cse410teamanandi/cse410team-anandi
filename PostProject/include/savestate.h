#ifndef SAVESTATE_H_
#define SAVESTATE_H_

#include <stdbool.h>

extern int save_called;

bool chk_file(const char *filname);
void save(char *origin, char *filename, struct Product * currentProduct, struct Filter * currentFilter);
void overwrite(char *origin, char *filename, struct Product * currentProduct, struct Filter * currentFilter);

#endif
