#ifndef HELPERS_H_
#define HELPERS_H_

#include <stdbool.h>
#include "structures.h"

void countProducts(struct Product* products);

void printProducts(struct Product *products);

#endif
