#ifndef MENU_H_
#define MENU_H_

#include <stdbool.h>

extern struct Product *FIRST;
extern char *originFile;

void help();

void run(struct Product *initialProducts, struct Filter *filtersList, char *inFile);

#endif
