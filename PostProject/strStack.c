#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "include/strStack.h"

stack *init() {
  stack *st_frame = malloc(sizeof(*st_frame));
  if (st_frame)
  {
    st_frame->head = NULL;
    st_frame->stackSize = 0;
  }
  return st_frame;
}

char* copy(char *inp) {
  char *tmp = malloc(strlen(inp)+1);
  if (tmp) {
    strcpy(tmp, inp);
    tmp[strlen(inp)+1] = '\0'; }
  return tmp;
}

void push(stack *theStack, char *value) {
  node *entry = malloc(sizeof *entry);
  if (entry)
  {
    entry->str = copy(value);
    entry->next = theStack->head;
    theStack->head = entry;
    theStack->stackSize++;
  }
  else
  {
    fprintf(stderr, "Error Type: Push Error......Exiting\n");
    exit(EXIT_FAILURE);
  }
}

char* top(stack *theStack) {
  if (theStack && theStack->head)
    return theStack->head->str;
  else
    return NULL;
}

char* pop(stack *theStack) {
  if (theStack->head != NULL)
  {
    node *tmp = theStack->head;
    theStack->head = theStack->head->next;
    return tmp->str;
    free(tmp->str);
    free(tmp);
    theStack->stackSize--;
  }
  else {
    return NULL;
  }
}

void clear(stack *theStack) {
  while (theStack != NULL)
    pop(theStack);
}

void destStack(stack **theStack) {
  clear(*theStack);
  free(*theStack);
}
