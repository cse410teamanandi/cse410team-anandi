#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include "include/structures.h"
#include "include/strStack.h"
#include "include/menu.h"
#include "include/helpers.h"

void remove_chars(char * string, char c) {
  char * pr = string, *pw = string;
  while(*pr) {
    *pw = *pr++;
    pw += (*pw != c);
  }
  *pw = '\0';
}

bool check_tag (char * string) {
  if( strcmp(string, "RECALLS_DATA") == 0 || strcmp(string, "PRODUCT") == 0 || strcmp(string, "DATE") == 0 || strcmp(string, "BRAND_NAME") == 0 || strcmp(string, "REASON") == 0
  || strcmp(string, "PRODUCT_DESCRIPTION") == 0 || strcmp(string, "PHOTOS_LINK") == 0 || strcmp(string, "COMPANY") == 0 || strcmp(string, "COMPANY_RELEASE_LINK") == 0 ) {
    return true;
  }
  else { return false; }
}

bool check_end (char * string) {
  if( strcmp(string, "/RECALLS_DATA") == 0 || strcmp(string, "/DATE") == 0 || strcmp(string, "/BRAND_NAME") == 0 || strcmp(string, "/REASON") == 0
  || strcmp(string, "/PRODUCT_DESCRIPTION") == 0 || strcmp(string, "/PHOTOS_LINK") == 0 || strcmp(string, "/COMPANY") == 0 || strcmp(string, "/COMPANY_RELEASE_LINK") == 0 ) {
    return true;
  }
  else { return false; }
}

int main(int argc, char * argv[]){
  const char delimiters[] = "<>";
  char filestring[999] = "";
  FILE * fp;
  char * infile;
  char * tokens;

  if (argc == 1) {
      fprintf(stderr, "Needs a filename argument.\n");
      exit(EXIT_FAILURE);
  }
  if (argc < 2 || argc > 2) {
     fprintf(stderr, "Usage: %s filename.xml\n", argv[0]);
     return 1;
   }
   infile = argv[1];

   fp = fopen(infile, "r");
   if(fp == NULL) {
     fprintf (stderr, "File error: Maybe DNE?\n");
     return -1;
   }
   else {
     char ch;
     while ( (ch = (char) fgetc(fp)) != EOF ) {
       strncat(filestring, &ch, 1);
     }
    fclose(fp);
   }

   remove_chars(filestring, '\n');

   tokens = strtok(filestring, delimiters);
   stack *tokStack = init();
   bool first_flag = true;
   struct Product *first;
   while(tokens != NULL)
   {
      if( strcmp(tokens, "RECALLS_DATA") == 0 || strcmp(tokens, "PRODUCT") == 0 || strcmp(tokens, "DATE") == 0 || strcmp(tokens, "BRAND_NAME") == 0 || strcmp(tokens, "REASON") == 0
      || strcmp(tokens, "PRODUCT_DESCRIPTION") == 0 || strcmp(tokens, "PHOTOS_LINK") == 0 || strcmp(tokens, "COMPANY") == 0 || strcmp(tokens, "COMPANY_RELEASE_LINK") == 0 ) {
        push(tokStack, tokens);
        tokens = strtok(NULL, delimiters);
      }
      else if( strcmp(tokens, "/RECALLS_DATA") == 0 || strcmp(tokens, "/DATE") == 0 || strcmp(tokens, "/BRAND_NAME") == 0 || strcmp(tokens, "/REASON") == 0
      || strcmp(tokens, "/PRODUCT_DESCRIPTION") == 0 || strcmp(tokens, "/PHOTOS_LINK") == 0 || strcmp(tokens, "/COMPANY") == 0 || strcmp(tokens, "/COMPANY_RELEASE_LINK") == 0 ) {
        push(tokStack, tokens);
        tokens = strtok(NULL, delimiters);
      }
      else if( strcmp(tokens, "/PRODUCT") == 0 ) {
        if(first_flag == true) {
          pop(tokStack);
          char *photo_link = check_tag(top(tokStack)) == true ? "" : pop(tokStack);
          pop(tokStack);
          pop(tokStack);
          char *rel_link = check_tag(top(tokStack)) == true ? "" : pop(tokStack);
          pop(tokStack);
          pop(tokStack);
          char *company = check_tag(top(tokStack)) == true ? "" : pop(tokStack);
          pop(tokStack);
          pop(tokStack);
          char *reason = check_tag(top(tokStack)) == true ? "" : pop(tokStack);
          pop(tokStack);
          pop(tokStack);
          char *prod_descript = check_tag(top(tokStack)) == true ? "" : pop(tokStack);
          pop(tokStack);
          pop(tokStack);
          char *brand = check_tag(top(tokStack)) == true ? "" : pop(tokStack);
          pop(tokStack);
          pop(tokStack);
          char *date = check_tag(top(tokStack)) == true ? "" : pop(tokStack);
          pop(tokStack);
          pop(tokStack);
          first = create_product(date, brand, prod_descript, reason, company, rel_link, photo_link);
          first_flag = false;
          tokens = strtok(NULL, delimiters);
        }
        else {
          pop(tokStack);
          char *photo_link = check_tag(top(tokStack)) == true ? "" : pop(tokStack);
          pop(tokStack);
          pop(tokStack);
          char *rel_link = check_tag(top(tokStack)) == true ? "" : pop(tokStack);
          pop(tokStack);
          pop(tokStack);
          char *company = check_tag(top(tokStack)) == true ? "" : pop(tokStack);
          pop(tokStack);
          pop(tokStack);
          char *reason = check_tag(top(tokStack)) == true ? "" : pop(tokStack);
          pop(tokStack);
          pop(tokStack);
          char *prod_descript = check_tag(top(tokStack)) == true ? "" : pop(tokStack);
          pop(tokStack);
          pop(tokStack);
          char *brand = check_tag(top(tokStack)) == true ? "" : pop(tokStack);
          pop(tokStack);
          pop(tokStack);
          char *date = check_tag(top(tokStack)) == true ? "" : pop(tokStack);
          pop(tokStack);
          pop(tokStack);
          struct Product *newProduct = create_product(date, brand, prod_descript, reason, company, rel_link, photo_link);
          addProductToList(first, newProduct);
          tokens = strtok(NULL, delimiters);
        }
      }
      else if(strcmp(tokens, "/RECALLS_DATA") == 0) {
        clear(tokStack);
	destStack(&tokStack);	
        tokens = strtok(NULL, delimiters);
      }
      else {
        if(strstr(tokens, "[CDATA[") != NULL) {
          char *initbuffer = strstr(tokens, "[CDATA[") + 8;
          char *endbuffer = malloc(strlen(initbuffer)+1);
          strcpy(endbuffer, initbuffer);
          endbuffer[strlen(initbuffer)-3] = '\0';
          push(tokStack, endbuffer);
          tokens = strtok(NULL, delimiters);
          free(endbuffer);
        }
        else {
          push(tokStack, tokens);
          tokens = strtok(NULL, delimiters);
        }
      }
   }

  //  help();
  //  printProduct(first);
  struct Filter* filtersList = create_filter("head","head");
  run(first, filtersList, infile);

  return 0;
}
