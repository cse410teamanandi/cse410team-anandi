#include "include/structures.h"
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include "CUnit.h"
#include "Basic.h"


void create_product_test1(void) {
  char *date = "Wed, 03 May 2017 14:25:00 -0400";
  char *name = "Wingman35";
  char *description = "Catheters";
  char *reason = "Tip splitting or separation";
  char *company = "ReFlow Medical";
  char *release = "https://www.fda.gov/Safety/Recalls/ucm556525.htm";
  char *photos = "";
  struct Product *testProduct = create_product(date, name, description, reason,
    company, release, photos);
  CU_ASSERT_STRING_EQUAL( date, testProduct->date);
  CU_ASSERT_STRING_EQUAL( name, testProduct->name);
  CU_ASSERT_STRING_EQUAL( description, testProduct->description);
  CU_ASSERT_STRING_EQUAL( reason, testProduct->reason);
  CU_ASSERT_STRING_EQUAL( company, testProduct->company);
  CU_ASSERT_STRING_EQUAL( release, testProduct->release);
  CU_ASSERT_STRING_EQUAL( photos, testProduct->photos);

  CU_ASSERT_TRUE(testProduct->next == NULL);
  CU_ASSERT_TRUE(testProduct->head == testProduct);

  free(testProduct);

}

void addProductToList_test1(void) {
  char *date = "Wed, 03 May 2017 14:25:00 -0400";
  char *name = "Wingman35";
  char *description = "Catheters";
  char *reason = "Tip splitting or separation";
  char *company = "ReFlow Medical";
  char *release = "https://www.fda.gov/Safety/Recalls/ucm556525.htm";
  char *photos = "";
  struct Product *testProduct = create_product(date, name, description, reason,
    company, release, photos);
  struct Product *newProduct = create_product("Sun, 29 Oct 2016 12:00:00 -400",
  "name", "desc", "reason","company", "release", "photos");

  addProductToList(testProduct, newProduct);

  CU_ASSERT_TRUE(testProduct->next == newProduct);
  CU_ASSERT_TRUE(newProduct->head == testProduct);

  free(testProduct);
  free(newProduct);

}

void addProductToList_test2(void) {
  char *date = "Wed, 03 May 2017 14:25:00 -0400";
  char *name = "Wingman35";
  char *description = "Catheters";
  char *reason = "Tip splitting or separation";
  char *company = "ReFlow Medical";
  char *release = "https://www.fda.gov/Safety/Recalls/ucm556525.htm";
  char *photos = "";
  struct Product *testProduct = create_product(date, name, description, reason,
    company, release, photos);
  struct Product *newProduct = create_product("Sun, 29 Oct 2016 12:00:00 -400",
  "name", "desc", "reason","company", "release", "photos");
  struct Product *anotherOne = create_product("a date", "a name", "a desc",
  "a reason", "a company",
                                              "a release", "some photos");

  addProductToList(testProduct, newProduct);
  addProductToList(testProduct, anotherOne);

  CU_ASSERT_TRUE(testProduct->next == newProduct);
  CU_ASSERT_TRUE(newProduct->head == testProduct);

  CU_ASSERT_TRUE(newProduct->next == anotherOne);
  CU_ASSERT_TRUE(anotherOne->head == testProduct);


  free(testProduct);
  free(newProduct);
  free(anotherOne);
}

void create_filter_test1(void) {
  struct Filter *testFilter = create_filter("this is a keyword",
  "this is a field");

  CU_ASSERT_STRING_EQUAL("this is a keyword", testFilter->keyword);
  CU_ASSERT_STRING_EQUAL("this is a field", testFilter->field);

  free(testFilter);
}

void addFilterToList_test1(void) {
  struct Filter *testFilter = create_filter("this is a keyword",
  "this is a field");
  struct Filter *newFilter = create_filter("this is another keyword",
  "this is another field");

  addFilterToList(testFilter, newFilter);

  CU_ASSERT_TRUE(testFilter->next == newFilter);
  CU_ASSERT_TRUE(newFilter->head == testFilter);

  free(testFilter);
  free(newFilter);

}

void addFilterToList_test2(void) {
  struct Filter *testFilter = create_filter("this is a keyword", "this is a field");
  struct Filter *newFilter = create_filter("this is another keyword", "this is another field");
  struct Filter *anotherNewFilter = create_filter("last key", "last field");

  addFilterToList(testFilter, newFilter);
  addFilterToList(testFilter, anotherNewFilter);

  CU_ASSERT_TRUE(testFilter->next == newFilter);
  CU_ASSERT_TRUE(newFilter->head == testFilter);
  CU_ASSERT_TRUE(anotherNewFilter->head == testFilter);
  CU_ASSERT_TRUE(newFilter->next == anotherNewFilter);

  free(testFilter);
  free(newFilter);
  free(anotherNewFilter);

}

void add_filter_test1(void) {
  struct Product *prod1 = create_product("1", "1", "1", "1", "1", "1", "1");
  struct Product *prod2 = create_product("1", "1", "1", "1", "1", "1", "1");
  struct Product *prod3 = create_product("2", "2", "2", "2", "2", "2", "2");

  addProductToList(prod1, prod2);
  addProductToList(prod1, prod3);

  CU_ASSERT_TRUE(prod1->head == prod2->head);
  CU_ASSERT_TRUE(prod1->head == prod3->head);
  CU_ASSERT_TRUE(prod1->head == prod1);

  struct Product *finalProducts;
  finalProducts = add_filter(prod1, NULL, "1", "ALL", true);

  while (finalProducts != NULL){
    CU_ASSERT_STRING_EQUAL(finalProducts->name, "1");
    finalProducts = finalProducts->next;
  }

  struct Product *notFinalProducts;
  notFinalProducts = add_filter(prod1, NULL, "1", "ALL", false);

  while (notFinalProducts != NULL){
    CU_ASSERT_STRING_NOT_EQUAL(notFinalProducts->name, "1");
    notFinalProducts = notFinalProducts->next;
  }

  free(prod1);
  free(prod2);
  free(prod3);
  free(finalProducts);
  free(notFinalProducts);
}

void add_filter_test2(void) {
  struct Product *prod1 = create_product("1", "1", "1", "1", "1", "1", "1");
  struct Product *prod2 = create_product("1", "1", "1", "1", "1", "1", "1");
  struct Product *prod3 = create_product("2", "2", "2", "2", "2", "2", "2");
  struct Product *prod4 = create_product("2", "2", "2", "2", "1", "2", "2");

  addProductToList(prod1,prod2);
  addProductToList(prod1,prod3);
  addProductToList(prod1,prod4);

  CU_ASSERT_TRUE((prod1->head == prod1) &&
  (prod1 == prod2->head) && (prod1 == prod3->head) && (prod1 == prod4->head));

  struct Product *finalProducts = add_filter(prod1, NULL, "1", "ANY", true);

  while (finalProducts != NULL){
    CU_ASSERT_STRING_EQUAL(finalProducts->company, "1");
    finalProducts = finalProducts->next;
  }

  struct Product *notFinalProducts = add_filter(prod1, NULL, "1", "ANY", false);

  while (notFinalProducts != NULL){
    CU_ASSERT_STRING_NOT_EQUAL(notFinalProducts->company, "1");
    notFinalProducts = notFinalProducts->next;
  }

  free(prod1);
  free(prod2);
  free(prod3);
  free(prod4);
  free(finalProducts);
  free(notFinalProducts);
}

void add_filter_test3(void) {
  struct Product *prod1 = create_product("this is a date","name","description","reason",
  "company","release","photos");
  struct Product *prod2 = create_product("1", "1", "1", "1", "1", "1", "1");
  struct Product *prod3 = create_product("this is a date as well", "1", "1", "1", "1", "1", "1");

  addProductToList(prod1,prod2);
  addProductToList(prod1,prod3);

  struct Product *finalProducts = add_filter(prod1, NULL, "this is a date", "DATE", true);

  while (finalProducts != NULL){
    CU_ASSERT_TRUE(strstr(finalProducts->date,"this is a date") != NULL);
    finalProducts = finalProducts->next;
  }

  struct Product *notFinalProducts = add_filter(prod1, NULL, "this is a date", "DATE", false);

  while (notFinalProducts != NULL){
    CU_ASSERT_TRUE(strstr(notFinalProducts->date,"this is a date") == NULL);
    notFinalProducts = notFinalProducts->next;
  }

  free(prod1);
  free(prod2);
  free(prod3);
  free(finalProducts);
  free(notFinalProducts);
}

/* The main() function for setting up and running the tests.
 * Returns a CUE_SUCCESS on successful running, another
 * CUnit error code on failure.
 */
int main()
{
   CU_pSuite Suite = NULL;

   /* initialize the CUnit test registry */
   if (CUE_SUCCESS != CU_initialize_registry()) { return CU_get_error(); }

   /* add a suite to the registry */
   Suite = CU_add_suite("Suite_of_tests_with_valid_inputs", NULL, NULL);
   if (NULL == Suite) {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* add the tests to Suite */
   if (
        (NULL == CU_add_test(Suite, "create_product", create_product_test1)) ||
        (NULL == CU_add_test(Suite, "add single product to list", addProductToList_test1)) ||
        (NULL == CU_add_test(Suite, "add multiple products to list", addProductToList_test2)) ||
        (NULL == CU_add_test(Suite, "create_filter", create_filter_test1)) ||
        (NULL == CU_add_test(Suite, "add single filter to filter list", addFilterToList_test1)) ||
        (NULL == CU_add_test(Suite, "add_filter - ALL", add_filter_test1)) ||
        (NULL == CU_add_test(Suite, "add multiple filters to list", addFilterToList_test2)) ||
        (NULL == CU_add_test(Suite, "add filter - ANY", add_filter_test2)) ||
        (NULL == CU_add_test(Suite, "add filter - DATE", add_filter_test3))

      )
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   CU_basic_set_mode(CU_BRM_VERBOSE);
   CU_basic_run_tests();

   /* Run all tests using automated interface, with output to 'test-Results.xml' */
  //  CU_set_output_filename("test");
  //  CU_automated_run_tests();

   CU_cleanup_registry();
   return CU_get_error();
}
