#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <stdbool.h>
#include "include/structures.h"
#include "include/menu.h"
#include "include/helpers.h"
#include "include/savestate.h"

void help(){
  // This function will be called by main if help is typed
  // help will print all possible commands in the console

  printf("%s\n\n", "Please enter one of the following commands:");
  printf("\t%s\n", "FILTER:\tfilter \"<keyword>\" <in> <field>");
  printf("\t\t%s\n", "where \"<keyword>\" is the word you are searching for,\n\t\t <in> may be either 'in' or 'notin',\n\t\t and <field> may be any of the following.");
  printf("\t\t%s\n\n", "DATE, BRAND_NAME, PRODUCT_DESCRIPTION, REASON,\n\t\tCOMPANY, COMPANY_RELEASE_LINK, or PHOTOS_LINK");
  printf("\t%s\n\n", "RESET:\treset");
  printf("\t%s\n\n", "SAVE:\tsave <filename>\n\t\twhere <filename> is a desired file name");
  printf("\t%s\n\n", "OVERWRITE: overwrite <filename>\n\t\twhere <filename> is the name of an already existing xml file");
  printf("\t%s\n\n","COUNT:\tcount");
  printf("\t%s\n\n","PRINT:\tprint");
  printf("\t%s\n\n","HELP:\thelp");
  printf("\t%s\n\n","QUIT:\tquit");

}

void run(struct Product *initialProducts, struct Filter* filtersList, char *inFile){
  struct Product *FIRST;
  memcpy(&FIRST,&initialProducts,sizeof(struct Product));
  char *originFile;
  memcpy(&originFile, &inFile, strlen(inFile)+1);
  char command[40];
  bool badcall = false;
  while(true){
    printf("Please enter a command. Say 'help' for more info\n? ");
    fgets(command,40,stdin);
    command[strcspn(command,"\n")] = '\0';
    badcall = false;

    if (strstr(command,"filter") != NULL){
      //filtering
      char keyword[50];
      char *start;
      char *end;
      bool inout = true;
      if (strstr(command,"\"") != NULL){
        start = strstr(command,"\"");
      }
      else {
        badcall = true;
      }
      if (!badcall && strstr(start+1,"\"") != NULL){
        end = strstr(start+1,"\"");
      }
      else {
        badcall = true;
      }

      memset(keyword,'\0',50);
      if (!badcall) {
        strncpy(keyword, start+1, end-start-1);
      }

      if (!badcall && strstr(end,"notin") != NULL){
          inout = false;
      }
      else {
        if (!badcall && strstr(end,"in") != NULL){
          inout = true;
        }
        else {
          badcall = true;
        }
      }
      char * field;
      if (!badcall && strstr(end,"DATE") != NULL){
        field = "DATE";
      }
      else if (!badcall && strstr(end,"BRAND_NAME") != NULL){
        field = "BRAND_NAME";
      }
      else if (!badcall && strstr(end,"PRODUCT_DESCRI") != NULL){
        field = "PRODUCT_DESCRIPTION";
      }
      else if (!badcall && strstr(end,"REASON") != NULL){
        field = "REASON";
      }
      else if (!badcall && strstr(end,"COMPANY_RE") != NULL) {
        field = "COMPANY_RELEASE_LINK";
      }
      else if (!badcall && strstr(end,"COMPANY") != NULL){
        field = "COMPANY";
      }
      else if (!badcall && strstr(end,"PHOTOS_LINK") != NULL){
        field = "PHOTOS_LINK";
      }
      else if (!badcall && strstr(end,"ANY") != NULL){
        field = "ANY";
      }
      else if (!badcall && strstr(end,"ALL") != NULL){
        field = "ALL";
      }
      else {
        badcall = true;
      }
      if (!badcall && field != NULL && keyword != NULL){
        initialProducts = add_filter(initialProducts,filtersList, keyword, field, inout);
      }
      else {
        printf("%s\n", "invalid input - try again!");
      }

    }
    else if (strcmp(command,"reset") == 0){
      // call reset

      struct Product *temp;
      struct Product *temp2 = initialProducts->head;
      initialProducts = initialProducts->head;
      while (initialProducts != NULL){
        temp = initialProducts;
        initialProducts = initialProducts->next;
        free(temp);
      }
      initialProducts = temp2;
      memcpy(&initialProducts,&FIRST,sizeof(struct Product));
    }
    else if (strstr(command,"save") != NULL){
      printf("Calling save on %s : check error warnings if any, else save successful\n", &command[5] );
      save(originFile, &command[5], initialProducts, filtersList);
    }
    else if (strstr(command,"overwrite") != NULL){
      printf("Calling overwrite on %s : check error warnings if any, else overwrite successful\n", &command[10] );
      overwrite(originFile, &command[10], initialProducts, filtersList);
    }
    else if (strcmp(command,"count") == 0){
      // call count
      countProducts(initialProducts);
    }
    else if (strcmp(command,"print") == 0){
      // call print
      printProducts(initialProducts);
    }
    else if (strcmp(command,"help") == 0){
      help();
    }
    else if (strcmp(command,"quit") == 0){
      if(save_called == 0) {
        printf("WARNING : save/overwrite has never been called! Are you sure you want to quit? (y/n): \n");
        char prompt[5];
        fgets(prompt, 5, stdin);
        prompt[strcspn(prompt, "\n")] = '\0';
        if (strstr(prompt, "y") != NULL) {
          break;
        }
        else{ }
      }
      else{
	break;
      }
    }
    else {
      printf("%s\n", "Failed to match a command. Type help for more info.");
    }
  }
}
