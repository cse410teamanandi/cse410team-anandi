#include "include/helpers.h"
#include "include/structures.h"
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

void countProducts(struct Product* products){
  int count = 0;
  if (products == NULL){

  }
  else {
    products = products->head;
    while (products != NULL){
      count++;
      products = products->next;
    }
  }
  printf("%s%d\n","The current number of Products is: ", count);
}

void printProducts(struct Product *products){
  printf("%s\n", "*******************************************************************************");

  int count = 1;
  if (products == NULL){
    printf("%s\n","There are currently no products to print!" );
    printf("%s\n", "*******************************************************************************");
  }
  else {
    products = products->head;
    while (products != NULL){
      printf("%s%d%s\n", "Product ",count, ": ");
      printf("%s%s\n", "\tDate: ",products->date);
      printf("%s%s\n", "\tName: ",products->name);
      printf("%s%s\n", "\tDescription: ",products->description);
      printf("%s%s\n", "\tReason: ",products->reason);
      printf("%s%s\n", "\tCompany: ",products->company);
      printf("%s%s\n", "\tRelease Link: ",products->release);
      printf("%s%s\n", "\tPhotos Link: ",products->photos);
      printf("%s\n", "*******************************************************************************");
      count++;
      products = products->next;

    }
  }
}
